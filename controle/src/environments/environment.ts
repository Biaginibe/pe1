// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyCAhtyNUpORTGTKRZS0hiQ7FQuDy5_VjkQ",
    authDomain: "controle-pe1.firebaseapp.com",
    projectId: "controle-pe1",
    storageBucket: "controle-pe1.appspot.com",
    messagingSenderId: "225479195590",
    appId: "1:225479195590:web:c1aec82131956eb86f90c7",
    measurementId: "G-S4VBPCK05C"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
