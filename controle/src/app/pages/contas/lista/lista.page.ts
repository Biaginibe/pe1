import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { ContaService } from '../service/conta.service';

@Component({
  selector: 'lista',
  templateUrl: './lista.page.html',
  styleUrls: ['./lista.page.scss'],
})
export class ListaPage implements OnInit {
  listaContas;
  tipo;
  constructor(private service: ContaService, private alert: AlertController, private router: Router) { }

  
  ngOnInit() {
    const url = this.router.url;
    const tipo = url.split('/')[2]
    this.tipo = tipo;
    this.service.lista(tipo).subscribe((x) => (this.listaContas = x));
  }

  async remove(conta) {
    const confirm = await this.alert.create({
      header: 'Remover conta',
      message: 'Deseja realmente deletar esta conta?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
        },
        {
          text: 'Deletar',
          handler: () => this.service.remove(conta),
        },
      ],
    });
    confirm.present();
  }

  async edita(conta) {
    const confirm = await this.alert.create({
      header: 'Editar conta',
      inputs: [
        {
          name: 'parceiro',
          value: conta.parceiro,
          placeholder: 'Parceiro comercial',
        },
        {
          name: 'descricao',
          value: conta.descricao,
          placeholder: 'Descrição',
        },
        {
          name: 'valor',
          value: conta.valor,
          type: 'number',
          placeholder: 'Valor',
        },
      ],
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
        },
        {
          text: 'Enviar',
          handler: (data) => {
            const merge = {...conta, ...data}
            this.service.edita(merge);
          },
        },
      ],
    });
    confirm.present();
  }
}