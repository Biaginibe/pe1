import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoginService } from '../service/login.service';

@Component({
  selector: 'register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  registerForm: FormGroup;

  constructor(
      private builder: FormBuilder,
      private service: LoginService,
    ) { }

  ngOnInit() {
    this.registerForm = this.builder.group({
      nome: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(19)]],
      snome: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(19)]],
      email: ['', [Validators.email, Validators.required]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirm_Password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  createUser(){
    const user = this.registerForm.value;
    this.service.createUser(user);
  }
}
